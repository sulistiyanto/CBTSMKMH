package com.tubandev.cbtclientsmkmh;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        new SplashRun().start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    class SplashRun extends Thread {
        SplashRun() {
        }

        public void run() {
            try {
                SplashRun.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                startActivity(new Intent(SplashScreenActivity.this, InputDomainActivity.class));
            }
        }
    }
}
